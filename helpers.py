

def process_result(result, forge, formatter, data_key = None):
	if result is not None:
		resultdata = result.json()
		if data_key is not None:
			resultdata = resultdata.get(data_key)
		
		result.data = [formatter(r, forge) for r in resultdata]
		# print(result)
	else:
		return
