from forge import Forge
from result import Result
import json
from helpers import process_result


def format_result(r, forge):
	return Result(
		r.get("id"),
		forge,
		r.get("full_name"),
		r.get("html_url"),
		r.get("description")
	)

class Gogs(Forge):

	BASE_URL = "https://gogs.io"

	def __init__(self):
		super().__init__("Gogs", "", self.BASE_URL + "/api/v1/repos/search?q={query}")
		self.url = self.BASE_URL
		self.instanceof = "Gogs"

	# https://try.gitea.io/api/swagger#/repository/repoSearch
	def searchfor(self, query:str, session=None):
		def process(result, *args, **kwargs):
			return process_result(result, self, format_result, data_key="data")
		return super().searchfor(query, session=session, response_hook=process)



class Notabug(Gogs):
	BASE_URL = "https://notabug.org"
	def __init__(self):
		super().__init__()
		self.name = "Notabug"