from forge import Forge
from result import Result
import json
from helpers import process_result

def format_result(r, forge):
	return Result(
		r.get("id"),
		forge,
		r.get("full_name"),
		r.get("html_url"),
		r.get("description")
	)

class GitHub(Forge):

	def __init__(self):
		super().__init__("GitHub", "https://github.com/search?q={query}", "https://api.github.com/search/repositories?q={query}")
		self.url = "https://github.com"

	def searchfor(self, query:str, session=None):
		def process(result, *args, **kwargs):
			return process_result(result, self, format_result, data_key="items")
		return super().searchfor(query, session=session, response_hook=process)
		
