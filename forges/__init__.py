from forge import Forge

from forges.github import GitHub
from forges.gitlab import *
from forges.gitea import *
from forges.gogs import *
from forges.launchpad import *

# sourcehut = Forge("SourceHut", "https://sr.ht/projects?search={query}&sort=recently-updated")

# notabug = Forge("Notabug", "https://notabug.org/explore/repos?q={query}", apisearchurl)

all_forges = [Codeberg(), OpenDev(),  Gitea(), Disroot(), Notabug(), Launchpad(), Sprinternet(), Autonomic(), GitLab(), GitHub()]