from forge import Forge
from result import Result
import json
from helpers import process_result


def format_result(r, forge):
	return Result(
		r.get("id"),
		forge,
		r.get("name"),
		r.get("web_link"),
		r.get("summary")
	)

class Launchpad(Forge):

	BASE_URL = "https://api.launchpad.net"

	def __init__(self):
		super().__init__("Launchpad", "", self.BASE_URL + "/1.0/projects?ws.op=search&text={query}")
		self.url = "https://launchpad.net"
		self.instanceof = "Launchpad"

	# https://try.gitea.io/api/swagger#/repository/repoSearch
	def searchfor(self, query:str, session=None):
		def process(result, *args, **kwargs):
			return process_result(result, self, format_result, data_key="entries")
		return super().searchfor(query, session=session, response_hook=process)