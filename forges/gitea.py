from forge import Forge
from result import Result
import json
from helpers import process_result



def format_result(r, forge):
	return Result(
		r.get("id"),
		forge,
		r.get("full_name"),
		r.get("html_url"),
		r.get("description")
	)
	

class Gitea(Forge):

	BASE_URL = "https://gitea.com"

	def __init__(self):
		super().__init__("Gitea", "", self.BASE_URL + "/api/v1/repos/search?q={query}")
		self.url = self.BASE_URL
		self.instanceof = "Gitea"

	# https://try.gitea.io/api/swagger#/repository/repoSearch
	def searchfor(self, query:str, session=None):
		def process(result, *args, **kwargs):
			return process_result(result, self, format_result, data_key="data")
		return super().searchfor(query, session=session, response_hook=process)
	
		


class Codeberg(Gitea):
	BASE_URL = "https://codeberg.org"
	def __init__(self):
		super().__init__()
		self.name = "Codeberg"


class OpenDev(Gitea):
	BASE_URL = "https://opendev.org"
	def __init__(self):
		super().__init__()
		self.name = "opendev.org"


class Disroot(Gitea):
	BASE_URL = "https://git.disroot.org"
	def __init__(self):
		super().__init__()
		self.name = "git.disroot.org"

class Sprinternet(Gitea):
	BASE_URL = "https://git.sprinternet.at"
	def __init__(self):
		super().__init__()
		self.name = "Sprinternet"

class Autonomic(Gitea):
	BASE_URL = "https://git.autonomic.zone"
	def __init__(self):
		super().__init__()
		self.name = "qutonomic.zone"