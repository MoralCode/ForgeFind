from forge import Forge
from result import Result
import os
import json
from constants import USER_AGENT
from helpers import process_result

def format_result(r, forge):
	return Result(
		r.get("id"),
		forge,
		r.get("namespace").get("name") + "/" + r.get("name"),
		r.get("http_url_to_repo"),
		r.get("description")
	)

class GitLab(Forge):

	BASE_URL = "https://gitlab.com"
	TOKEN_NAME = "GITLAB_TOKEN"

	def __init__(self):
		super().__init__("GitLab", "https://gitlab.com/search?search={query}", self.BASE_URL + "/api/v4/search?scope=projects&search={query}")
		self.url = self.BASE_URL
		self.instanceof = "GitLab"


	# https://docs.gitlab.com/ee/api/search.html
	def searchfor(self, query:str, session=None):
		
		token = os.getenv(self.TOKEN_NAME)
		if token is not None:
			def process(result, *args, **kwargs):
				return process_result(result, self, format_result)
			return super().searchfor(query, session=session, response_hook=process, headers={"PRIVATE-TOKEN": token})
		
		return None
		
		# print(result)


class KDEInvent(GitLab):
	BASE_URL = "https://invent.kde.org"
	TOKEN_NAME = "KDE_GITLAB_TOKEN"

	def __init__(self):
		super().__init__()
		self.name = "KDE Invent"

class IEEESAOpen(GitLab):
	BASE_URL = "https://opensource.ieee.org"
	TOKEN_NAME = "IEEE_GITLAB_TOKEN"

	def __init__(self):
		super().__init__()
		self.name = "IEEE SA Open"