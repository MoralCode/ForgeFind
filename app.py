from flask import Flask, render_template, request
from forges import all_forges
from constants import REPO
import os
import requests
import requests_cache
from requests_futures.sessions import FuturesSession

session = None
if os.getenv("FLASK_ENV") == 'development':
	session = requests_cache.CachedSession('forge-cache')
else:
	session = FuturesSession()


app = Flask(__name__, static_folder='static',)

@app.route("/")
def index():
	return render_template('index.html', forges=all_forges, repourl=REPO)


@app.route("/search")
def search():
	# print(f"queried for {}")
	query=request.args.get('query')
	print("handling query: " + query)
	queries = []
	results = []
	# kick off all requests asynchronously
	for forge in all_forges:
		print("querying " + forge.name)
		# try:
		future = forge.searchfor(query, session=session)
		if future is not None:
			queries.append(future)
		# except Exception as e:
		# 	print(e)
	
	# then wait for all the results 
	for q in queries:
		res = q.result()
		if res is not None:
			try:
				results.extend(res.data)
			except Exception as e:
				print(e)
	# print(results)
	return render_template("results.html", results=results)