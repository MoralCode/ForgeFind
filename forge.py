from dataclasses import dataclass

import os
from constants import USER_AGENT
	

@dataclass
class Forge:
	"""a code forge
	"""
	name: str
	usersearchurl: str
	apisearchurl: str
	#  a user friendly url to the main page for this forge.
	url:str = ""
	# what software is this forge based on? (if any)
	instanceof:str = ""

	def query(self, url, session=None, response_hook=None, headers=USER_AGENT):
		if headers:
			headers = USER_AGENT.update(headers)
		return session.get(url, headers=headers, hooks={
			'response': response_hook,
		})
	
	# kick off an asynchronous request
	def searchfor(self, query:str, session=None, response_hook=None, headers=None):
		url = self.apisearchurl.format(query=query)
		
		# a basic check of the request was successful, wrapped around the hook provided by the individual forge
		def handle_response(result, *args, **kwargs):
			if result is None:
				response_hook(None, *args, **kwargs )
			elif result.status_code != 200:
				print("error fetching data from " + result.url + ": " + result.text)
				response_hook(None, *args, **kwargs )
			else:
				response_hook(result, *args, **kwargs )

		return self.query(url, session=session, response_hook=handle_response, headers=headers)
		
		
